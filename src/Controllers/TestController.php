<?php

namespace Dibs\Api\Controllers;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

class TestController
{
    public function home(Twig $twig, Request $request, Response $response, ?string $foo)
    {
        return $twig->render($response, 'home.twig', ['foo' => $foo]);
    }
}
