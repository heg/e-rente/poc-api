<?php

namespace Dibs\Api\Controllers;

use Dibs\Api\State\StateManager;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class StateController
{
    public function getCurrent(Response $response, StateManager $stateManager)
    {
        $states = $stateManager->getCurrent();
        $response->getBody()->write(json_encode($states));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }
}
