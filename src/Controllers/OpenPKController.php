<?php

namespace Dibs\Api\Controllers;

use Dibs\Api\Exceptions\ValidationFailedException;
use Dibs\Api\OpenPK\OpenPKManager;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Views\Twig;

class OpenPKController
{
    public function grantingIsOkay(Twig $twig, Response $response)
    {
        return $twig->render($response, 'grant-ok.twig');
    }

    public function options(Response $response)
    {
        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function getRegistry(Response $response, OpenPKManager $openpkManager)
    {
        $registry = $openpkManager->getRegistry(true);
        $response->getBody()->write(json_encode($registry));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function setPensionFunds(Request $request, Response $response, OpenPKManager $openpkManager)
    {
        $parsedBody = $request->getParsedBody();
        $ids        = $parsedBody['ids'] ?? [];

        if (is_string($ids)) {
            $ids = json_decode($ids);
        }

        if (!is_array($ids)) {
            throw new ValidationFailedException("'ids' doit être un tableau d'identifiant de caisses de pension'");
        }

        $openpkManager->savePensionFundSelection($ids);

        return $response;
    }

    public function getPensionFunds(Response $response, OpenPKManager $openpkManager)
    {
        $ids = $openpkManager->getPensionFundSelection();
        $response->getBody()->write(json_encode($ids));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function getNextGrant(Request $request, Response $response, OpenPKManager $openpkManager)
    {
        $nextRegistryInfo = $openpkManager->getNextGrantRegistryInfo();

        if (!$nextRegistryInfo) {
            return $response
                ->withHeader('Location', '/openpk/user/pension-funds/grants/ok')
                ->withStatus(302);
        }

        $query     = $request->getQueryParams();
        $result    = $query['result'] ?? false;

        if (!$result) {
            return $response
                ->withHeader('Location', $openpkManager->getGrantUrl($nextRegistryInfo))
                ->withStatus(302);
        }

            return $response
                ->withHeader('Location', '/openpk/user/pension-funds/grants')
                ->withStatus(302);
    }

    public function getNextConsent(Request $request, Response $response, OpenPKManager $openpkManager)
    {
        $nextRegistryInfo = $openpkManager->getNextConsentRegistryInfo();

        if (!$nextRegistryInfo) {
            return $response
                ->withHeader('Location', '/openpk/user/pension-funds/grants')
                ->withStatus(302);
        }

        $query = $request->getQueryParams();
        $code  = $query['code'] ?? false;
        $state = $query['state'] ?? false;

        if (!$code) {
            return $response
                ->withHeader('Location', $openpkManager->getAuthorizationUrl($nextRegistryInfo))
                ->withStatus(302);
        }

        if (!$openpkManager->isStateOkay($state)) {
            $response->getBody()->write('Invalid state');

            return $response
                ->withStatus(401);
        }

        $openpkManager->retrieveAccessToken($nextRegistryInfo, $code);

        return $response
            ->withHeader('Location', '/openpk/user/pension-funds/tokens')
            ->withStatus(302);
    }

    public function getPolicies(Response $response, OpenPKManager $openpkManager)
    {
        $policies = $openpkManager->getPolicies();

        $response->getBody()->write(json_encode($policies));
        return $response
            ->withStatus(200)
            ->withHeader('Content-Type', 'application/json');
    }
}
