<?php

namespace Dibs\Api\Controllers;

use Dibs\Api\Login\LoginManager;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class LoginController
{
    public function me(Response $response, LoginManager $loginManager)
    {
        $resourceOwner = $loginManager->getResourceOwner();

        $response->getBody()->write(json_encode($resourceOwner->toArray()));

        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function token(Request $request, Response $response, LoginManager $loginManager)
    {
        $query   = $request->getQueryParams();
        $referer = $query['flash_uri'] ?? false;
        $code    = $query['code'] ?? false;
        $state   = $query['state'] ?? false;


        if (!$code) {
            if ($referer) {
                $_SESSION['flash_uri'] = $referer;
            }
            return $response
                ->withHeader('Location', $loginManager->getAuthorizationUrl())
                ->withStatus(302);
        }

        $referer = $_SESSION['flash_uri'] ?? null;
        if (!$loginManager->isStateOkay($state)) {
            $loginManager->cancelAuthorizationAttempt();
            $response->getBody()->write('Invalid state');

            return $response
                ->withStatus(401);
        }

        $loginManager->retrieveAccessToken($code);

        $_SESSION['flash_uri'] = null;
        return $response
            ->withHeader('Location', $referer ?: $_ENV['INTERFACE_DOMAIN'] . $_ENV['INTERFACE_LOGIN_SUCCESS_PATH'])
            ->withStatus(302);
    }

    public function disconnect(Request $request, Response $response, LoginManager $loginManager)
    {
        $referer = $_SERVER['HTTP_REFERER'] ?? false;
        $loginManager->emptySession();

        $_SESSION['REFERER'] = $_SERVER['HTTP_REFERER'] ?? false;

        return $response
            ->withHeader('Location', $loginManager->getLogoutUri())
            ->withStatus(302);
    }

    public function redirect(Response $response)
    {
        $referer = $_SESSION['REFERER'] ?: $_ENV['INTERFACE_DOMAIN'];

        return $response
            ->withHeader('Location', $referer)
            ->withStatus(302);
    }
}
