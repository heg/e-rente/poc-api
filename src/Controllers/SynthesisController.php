<?php

namespace Dibs\Api\Controllers;

use Dibs\Api\Synthesis\SynthesisManager;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Dibs\Api\Synthesis\SimulationData;

class SynthesisController
{
    public function digest(Request $request, Response $response, SynthesisManager $SynthesisManager)
    {

        $query = $request->getQueryParams();

        $employmentLevel = isset($query['employmentLevel']) ? $query['employmentLevel'] : null;
        $effectiveDate   = isset($query['effectiveDate']) ? $query['effectiveDate'] : null;
        $declaredSalary  = isset($query['declaredSalary']) ? $query['declaredSalary'] : null;

        $simulation = null;
        if ($employmentLevel || $effectiveDate || $declaredSalary) {
            $simulation = new SimulationData([
            'employmentLevel' => $employmentLevel,
            'effectiveDate'   => $effectiveDate,
            'declaredSalary'  => $declaredSalary,
            ]);
        }

        $digestion = $SynthesisManager->digest($simulation);
        $response->getBody()->write(json_encode($digestion));

        return $response;
    }
}
