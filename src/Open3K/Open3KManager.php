<?php

namespace Dibs\Api\Open3K;

use Dibs\Api\User\UserManager;

class Open3KManager
{
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }

    public function getInfo()
    {
        return [
            'solde'   => $this->userManager->retrieveFromPrivateDb('open3k.solde'),
            'amount'  => $this->userManager->retrieveFromPrivateDb('open3k.amount'),
            'rate'    => $this->userManager->retrieveFromPrivateDb('open3k.rate'),
            'paydate' => $this->userManager->retrieveFromPrivateDb('open3k.paydate'),
        ];
    }

    public function saveInfo($solde, $amount, $rate, $paydate)
    {
        $this->userManager->appendToPrivateDb('open3k.solde', $solde);
        $this->userManager->appendToPrivateDb('open3k.amount', $amount);
        $this->userManager->appendToPrivateDb('open3k.rate', $rate);
        $this->userManager->appendToPrivateDb('open3k.paydate', $paydate);
    }

    public function savePaydate($paydate)
    {
        return $this->userManager->appendToPrivateDb('open3k.paydate', $paydate);
    }

    public function getPaydate()
    {
        return $this->userManager->retrieveFromPrivateDb('open3k.paydate');
    }
}
