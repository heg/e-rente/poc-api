<?php

namespace Dibs\Api\OpenPK;

use League\OAuth2\Client\Provider\GenericProvider;

class OAuthProvider extends GenericProvider
{
    protected $redirectGrantUri;

    /**
     * @param array $options
     * @param array $collaborators
     */
    public function __construct(array $options = [], array $collaborators = [])
    {
        $this->redirectGrantUri = $options['redirectGrantUri'] ?? '';

        parent::__construct($options, $collaborators);
    }

    public function getGrantUrl($registryInfo)
    {
        $uri = $registryInfo->openpkServices->grantUrl;
        $uri = parse_url($uri);
        $uri = $uri['scheme'] . '://' . $uri['host'] . $uri['path'] . '?'
        . (isset($uri['query']) ? $uri['query'] . '&' : '')
        . 'redirect_uri=' . urlencode($this->redirectGrantUri)
        . '&' . http_build_query(
            [
            'pension_id' => $registryInfo->id,
            'client_id'  => $_ENV['OPENPK_CLIENT_ID'],
            ]
        );

        return $uri;
    }
}
