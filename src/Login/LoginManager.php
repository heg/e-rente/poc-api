<?php

namespace Dibs\Api\Login;

use Dibs\Api\Exceptions\UnauthorizedException;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Token\AccessToken;
use Psr\Http\Message\ServerRequestInterface as Request;

class LoginManager
{
    protected $oAuthProvider;
    protected $resourceOwner;

    public function __construct(AbstractProvider $oAuthProvider)
    {
        $this->oAuthProvider = $oAuthProvider;
    }

    public function getProvider()
    {
        return $this->oAuthProvider;
    }

    public function getAuthorizationUrl()
    {
        $authorizationUrl        = $this->oAuthProvider->getAuthorizationUrl(['scope' => 'offline_access openid profile email']);
        $_SESSION['oauth2state'] = $this->oAuthProvider->getState();

        return $authorizationUrl;
    }

    public function isStateOkay(string $state = null)
    {
        return $state
        && isset($_SESSION['oauth2state'])
            && ($state == $_SESSION['oauth2state']);
    }

    public function cancelAuthorizationAttempt()
    {

        if (isset($_SESSION['oauth2state'])) {
            unset($_SESSION['oauth2state']);
        }
    }

    public function retrieveAccessToken(string $code)
    {
        $accessToken = $this->oAuthProvider->getAccessToken(
            'authorization_code',
            [
            'code' => $code,
            ]
        );

        $this->storeAccessToken($accessToken);
    }

    public function refreshAccessToken()
    {
        $accessToken = $this->getStoredAccessToken();
        try {
            $newAccessToken = $this->oAuthProvider->getAccessToken(
                'refresh_token',
                [
                'refresh_token' => $accessToken->getRefreshToken(),
                ]
            );
        } catch (\Exception $e) {
            throw new UnauthorizedException('Impossible de rafraichir le token: ' . $e->getMessage());
        }

        $this->storeAccessToken($newAccessToken);

        return $newAccessToken;
    }

    public function getValidAccessToken(Request $request = null)
    {
        $accessToken = $this->getStoredAccessToken($request);

        if ($accessToken->hasExpired()) {
            $_SESSION['ressource_owner'] = null;
            $accessToken                 = $this->refreshAccessToken();
        }

        return $accessToken;
    }

    public function isTokenValid(Request $request = null)
    {
        $accessToken = $this->getValidAccessToken($request);

        return !$accessToken->hasExpired();
    }

    public function getResourceOwner()
    {
        $accessToken = $this->getValidAccessToken();

        try {
            if (!$this->resourceOwner) {
                $this->resourceOwner = $_SESSION['ressource_owner'] ?? $this->oAuthProvider->getResourceOwner($accessToken);
                $_SESSION['ressource_owner'] = $this->resourceOwner;
            }
        } catch (\Exception $e) {
            throw new UnauthorizedException('Impossible de récupérer le propriétaire du token d\'accès: ' . $e->getMessage());
        }

        return $this->resourceOwner;
    }

    protected function storeAccessToken(AccessToken $accessToken)
    {
        $_SESSION['login_token'] = json_encode($accessToken);
    }

    protected function getStoredAccessToken(Request $request = null)
    {

        if ($request) {
            $accessToken = str_replace('Bearer ', '', $request->getHeader('Authorization')[0] ?? null);

            if ($accessToken) {
                $this->storeAccessToken(
                    new AccessToken(
                        [
                        'access_token' => str_replace('Bearer ', '', $request->getHeaderLine('Authorization') ?? null),
                        'expires'      => time() + 10,
                        ]
                    )
                );
            }
        }

        $tokens = $_SESSION['login_token'] ?? null;
        $tokens = json_decode($tokens, true);

        if (!$tokens) {
            throw new UnauthorizedException("Il n'y a pas de token");
        }

        $accessToken = new AccessToken($tokens);

        return $accessToken;
    }

    public function emptySession()
    {
        session_unset();
        session_regenerate_id(true);
    }

    public function getLogoutUri()
    {
        $logoutUri = $this->oAuthProvider->getLogoutUri();

        return $logoutUri;
    }
}
