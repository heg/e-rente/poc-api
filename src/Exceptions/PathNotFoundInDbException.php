<?php

namespace Dibs\Api\Exceptions;

use Dibs\Api\Exceptions\AbstractException;

/**
 * Exception lancée si le chemin n'existe pas dans la base de données
 */
class PathNotFoundInDbException extends AbstractException
{
    /**
     * Code de l'erreur: 422
     */
    const CODE = 422;
}
