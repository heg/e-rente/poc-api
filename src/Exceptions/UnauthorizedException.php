<?php

namespace Dibs\Api\Exceptions;

use Dibs\Api\Exceptions\AbstractException;

/**
 * Exception lancée si l'utilisateur n'est pas connecté alors qu'il devrait l'être
 */
class UnauthorizedException extends AbstractException
{
    /**
     * Code de l'erreur: 401
     */
    const CODE = 401;
}
