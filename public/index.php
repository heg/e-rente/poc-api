<?php

require __DIR__ . '/../vendor/autoload.php';

use Dibs\Api\Controllers\LoginController;
use Dibs\Api\Controllers\Open3KController;
use Dibs\Api\Controllers\OpenAKController;
use Dibs\Api\Controllers\OpenPKController;
use Dibs\Api\Controllers\StateController;
use Dibs\Api\Controllers\SynthesisController;
use Dibs\Api\Exceptions\ExceptionHandler;
use Dibs\Api\Http\CorsMiddleware;
use Dibs\Api\Http\JsonBodyParserMiddleware;
use Dibs\Api\Login\IsAuthentifiedMiddleware;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__ . '/..');
$dotenv->load();

// Configure PHP-DI container
$builder = new \DI\ContainerBuilder();
/**
 * Si jamais on veut compiler l'injection de code => en prod uniquement et
 * il faut vider le cache à chaque modification des injections
 */
// $builder->enableCompilation(__DIR__ . '/../cache');
$builder->addDefinitions(__DIR__ . '/../config/injections.php');
$container = $builder->build();

// Create App
$app = \DI\Bridge\Slim\Bridge::create($container);

// Initialise le gestionnaire de session
session_set_save_handler($container->get('SessionHandlerInterface'), true);
session_start();

// app CORS config
$app->add($container->get(CorsMiddleware::class));
$app->add($container->get(JsonBodyParserMiddleware::class));

// app error config
$errorMiddleware = $app->addErrorMiddleware(true, true, true);
$errorMiddleware->setDefaultErrorHandler($container->get(ExceptionHandler::class));

$routeParser = $app->getRouteCollector()->getRouteParser();
$container->set(Slim\Interfaces\RouteParserInterface::class, $routeParser);

// API Endpoints

/**
 * Permet de se connecter
 */
$app->get('/login/connect', [LoginController::class, 'token'])->setName('login');
/**
 * Permet de se déconnecter
 */
$app->get('/login/disconnect', [LoginController::class, 'disconnect']);
/**
 * Redirection vers l'interface si besoin (notamment appelé après la déconnexion)
 */
$app->get('/redirect', [LoginController::class, 'redirect']);

$app->options('[/{params:.*}]', [OpenPKController::class, 'options']);

/**
 * Groupe des endpoints necessitants d'être connecté (Cookie ou Authorization header)
 */
$app->group(
    '',
    function (RouteCollectorProxy $group) {
        /**
         * Récupère les informations de l'utilisateur connecté
         */
        $group->get('/me', [LoginController::class, 'me']);

        /**
         * Permet de récupérer l'état courant du compte
         */
        $group->get('/states', [StateController::class, 'getCurrent']);

        /**
         * Les groupes autour des requêtes de synthèse
         */
        $group->group(
            '/synthesis',
            function (RouteCollectorProxy $group) {
                $group->get('/digest', [SynthesisController::class, 'digest']);
            }
        );

        /**
         * Les groupes autour des requêtes OpenAK
         */
        $group->group(
            '/open3k',
            function (RouteCollectorProxy $group) {
                $group->get('/info', [Open3KController::class, 'getInfo']);
                $group->post('/info', [Open3KController::class, 'postInfo']);
            }
        );

        /**
         * Les groupes autour des requêtes OpenAK
         */
        $group->group(
            '/openak',
            function (RouteCollectorProxy $group) {
                $group->get('/test-connection', [OpenAKController::class, 'test']);
                $group->get('/pension-calculator/info', [OpenAKController::class, 'getPensionCalculatorInfo']);
                $group->post('/pension-calculator/info', [OpenAKController::class, 'postPensionCalculatorInfo']);
                $group->get('/pension-calculator', [OpenAKController::class, 'retrievePensionCalculator']);
            }
        );

        /**
         * Les groupes autour des requêtes OpenPK
         */
        $group->group(
            '/openpk',
            function (RouteCollectorProxy $group) {

                /**
                 * Récupère le registre des caisses de retraites
                 */
                $group->get('/pension-funds', [OpenPKController::class, 'getRegistry']);

                /**
                 * Permet de séléctionner les caisses de pensions de l'utilisateur
                 * - nécessite un tableau d'ids dans le body
                 * {
                 * "ids": [<string>]
                 * }
                 */
                $group->post('/user/pension-funds', [OpenPKController::class, 'setPensionFunds']);

                /**
                 * Récupère le tableau des identifiants de fond de pension
                 */
                $group->get('/user/pension-funds', [OpenPKController::class, 'getPensionFunds']);

                /**
                 * Récupère tous les tokens des différentes caisses de pension
                 */
                $group->get('/user/pension-funds/tokens', [OpenPKController::class, 'getNextConsent']);

                /**
                 * Vérifie tous les granting des fonds de pensions
                 */
                $group->get('/user/pension-funds/grants', [OpenPKController::class, 'getNextGrant']);

                /**
                 * Vérifie tous les granting des fonds de pensions
                 */
                $group->get('/user/pension-funds/grants/ok', [OpenPKController::class, 'grantingIsOkay']);

                /**
                 * Récupère le tableau des identifiants de fond de pension
                 */
                $group->get('/user/pension-funds/policies', [OpenPKController::class, 'getPolicies']);
            }
        );
    }
)->add($container->get(IsAuthentifiedMiddleware::class));

// let's roll !
$app->run();
